﻿// Created by Moritz on 03.02.2021

using System;
using School.Command;
using School.Lesson;

namespace School
{
    internal class Program
    {
        private static Program _instance;

        private Program()
        {
            _instance = this;
        }

        static void Main(string[] args)
        {
            Program program = new();
            program.Startup();
        }

        private void Startup()
        {
            Console.Clear();

            LessonManager lessonManager = new();
            lessonManager.RegisterLessons();

            CommandManager commandManager = new();
            commandManager.RegisterCommands();

            Beta();

            commandManager.StartLoop();
        }

        private static void Beta()
        {
            Console.Clear();

            Console.WriteLine("Hallo Frau Schmidt. Dieses Program ist noch nicht vollständig feritg.");
            Console.WriteLine(
                "Sie können über den Command 'lesson 2 <task>' das Arbeitsblatt vom 24. Februar aufrufen.");
            Console.WriteLine();
            Console.WriteLine("lesson 2 1 - Erste Aufgabe");
            Console.WriteLine("lesson 2 2 - Zweite Aufgabe");
            Console.WriteLine("lesson 2 3 - Dritte Aufgabe, usw...");
            Console.WriteLine();
        }


        public static Program GetInstance()
        {
            return _instance;
        }
    }
}