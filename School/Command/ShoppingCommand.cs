// Created by Moritz on 10.03.2021

using System;
using School.Util;

namespace School.Command
{
    public class ShoppingCommand : Command
    {
        public ShoppingCommand() : base("shop")
        {
            /*
             * shop list
             * shop add
             * shop clear
             */
        }

        public override void Execute(string[] args)
        {
            if (args.Length == 1)
                switch (args[0].ToLower())
                {
                    case "add":
                        Console.Clear();
                        ShoppingList.GetInstance().AddItem();
                        return;
                    case "clear":
                        Console.Clear();
                        ShoppingList.GetInstance().ClearList();
                        return;
                    case "list":
                        Console.Clear();
                        ShoppingList.GetInstance().ShowList();
                        return;
                }

            Message.Colored("Falscher Syntax: Benutze bitte 'shop <add | clear | list>'", ConsoleColor.Red);
            Console.WriteLine();
        }
    }
}