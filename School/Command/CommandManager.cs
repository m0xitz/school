// Created by Moritz on 05.02.2021

using System;
using System.Collections.Generic;
using System.Linq;

namespace School.Command
{
    public class CommandManager
    {
        private static CommandManager _instance;

        private List<Command> commands;
        private Dictionary<string, Command> commandHash;

        public CommandManager()
        {
            _instance = this;

            commands = new();
            commandHash = new();
        }


        public void RegisterCommands()
        {
            commands.Add(new ClearCommand());
            commands.Add(new StopCommand());
            commands.Add(new ColorCommand());
            commands.Add(new HelpCommand());
            commands.Add(new LessonCommand());
            commands.Add(new ShoppingCommand());

            foreach (var command in commands)
                commandHash.Add(command.GetName(), command);
        }

        public void StartLoop()
        {
            bool found;

            while (true)
            {
                string input = Console.ReadLine();
                if (input == null)
                    continue;

                string[] args = input.Split(" ");
                List<string> argsList = new(args);

                string commandName = args[0];

                argsList.RemoveAt(0);

                args = argsList.ToArray();

                found = false;

                foreach (var command in commands.Where(command =>
                    command.GetName().ToUpper().Equals(commandName.ToUpper())))
                {
                    command.Execute(args);
                    found = true;
                    break;
                }

                if (found)
                    continue;

                Console.WriteLine("Command does not exits. Please type 'help' for help.");
            }
        }


        public List<Command> GetCommands()
        {
            return commands;
        }

        public static CommandManager GetInstance()
        {
            return _instance;
        }
    }
}