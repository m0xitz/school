// Created by Moritz on 05.02.2021

using System;

namespace School.Command
{
    public class HelpCommand : Command
    {
        public HelpCommand() : base("help")
        {
        }

        public override void Execute(string[] args)
        {
            Console.WriteLine("List of all commands.");

            foreach (var command in CommandManager.GetInstance().GetCommands())
                Console.WriteLine($"- {command.GetName()} ({command.GetDescription()})");
        }
    }
}