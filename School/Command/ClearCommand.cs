// Created by Moritz on 05.02.2021

using System;

namespace School.Command
{
    public class ClearCommand : Command
    {
        public ClearCommand() : base("clear")
        {
        }

        public override void Execute(string[] args)
        {
            Console.Clear();
        }
    }
}