// Created by Moritz on 05.02.2021

using System;

namespace School.Command
{
    public class StopCommand : Command
    {
        public StopCommand() : base("stop")
        {
        }

        public override void Execute(string[] args)
        {
            Console.WriteLine("Program closed");
            Environment.Exit(0);
        }
    }
}