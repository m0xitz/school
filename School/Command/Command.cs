// Created by Moritz on 05.02.2021

using System;

namespace School.Command
{
    public abstract class Command
    {
        private readonly string _name;
        private readonly string _description;

        protected Command(string name, string description)
        {
            _name = name;
            _description = description;

            Console.WriteLine($"Command '{name}' registered.");
        }

        protected Command(string name)
        {
            _name = name;
            _description = "placeholder";

            Console.WriteLine($"Command '{name}' registered.");
        }

        public abstract void Execute(string[] args);

        public string GetName()
        {
            return _name;
        }

        public string GetDescription()
        {
            return _description;
        }
    }
}