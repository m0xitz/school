// Created by Moritz on 10.02.2021

using System;
using School.Lesson;

namespace School.Command
{
    public class LessonCommand : Command
    {
        public LessonCommand() : base("lesson")
        {
        }

        public override void Execute(string[] args)
        {
            Console.WriteLine();
            switch (args.Length)
            {
                case 2:
                    var lesson = LessonManager.GetInstance().GetLesson(Convert.ToInt32(args[0]));
                    int task = Convert.ToInt32(args[1]);

                    if (lesson == null)
                        Console.WriteLine("Das Aufgabenblatt wurde nicht gefunden.");
                    else
                        switch (task)
                        {
                            case 1:
                                Console.Clear();
                                lesson.FirstTask();
                                return;
                            case 2:
                                Console.Clear();
                                lesson.SecondTask();
                                return;
                            case 3:
                                Console.Clear();
                                lesson.ThirdTask();
                                return;
                            case 4:
                                Console.Clear();
                                lesson.FourthTask();
                                return;
                            case 5:
                                Console.Clear();
                                lesson.FifthTask();
                                return;
                            default:
                                Console.WriteLine($"Aufgabe {task} wurde nicht gefunden.");
                                break;
                        }

                    break;
                case 1 when args[0].ToLower().Equals("list"):
                    break;
                default:
                    Console.WriteLine("Falscher Syntax: Benutze bitte 'lesson <id> <task>'");
                    break;
            }

            Console.WriteLine("Verfügbare Aufgabenblätter:");
            foreach (var lessonItem in LessonManager.GetInstance().GetLessons())
                Console.WriteLine(
                    $"- {lessonItem.GetName()} ({lessonItem.GetCount()}) vom {lessonItem.GetDate():dd.MM.yyyy}");
        }
    }
}