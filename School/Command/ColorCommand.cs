// Created by Moritz on 05.02.2021

using System;

namespace School.Command
{
    public class ColorCommand : Command
    {
        public ColorCommand() : base("color")
        {
        }

        public override void Execute(string[] args)
        {
            if (args.Length == 0)
            {
                Console.WriteLine($"Die aktuelle Farbe der Console ist {Console.BackgroundColor}.");
                return;
            }

            var colorString = args[0];

            try
            {
                Console.BackgroundColor = (ConsoleColor) Enum.Parse(typeof(ConsoleColor), colorString, true);
                Console.WriteLine($"Die Farbe der Console wurde zu {Console.BackgroundColor} geändert.");
            }
            catch (ArgumentException)
            {
                Console.WriteLine($"{colorString} is not a member of the BackgroundColor enumeration.");
            }
        }
    }
}