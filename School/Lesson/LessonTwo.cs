// Created by Moritz on 13.02.2021

using System;
using School.Util;

namespace School.Lesson
{
    public class LessonTwo : Lesson
    {
        private readonly ShoppingList _list;

        public LessonTwo() : base("two", 2, new DateTime(2021, 2, 24))
        {
            _list = new ShoppingList();
        }

        public override void FirstTask()
        {
            Console.Write("Bitte geben Sie Ihren Namen ein: ");
            string name = Message.Colored(ConsoleColor.Blue);

            Console.Write("Wann haben Sie Geburtstag? (dd.MM.yyyy): ");
            string birth = Message.Colored(ConsoleColor.Blue) ?? "01.01.1970";
            string[] birthArray = birth.Split(".");

            int day = int.Parse(birthArray[0]);
            int month = int.Parse(birthArray[1]);
            int year = int.Parse(birthArray[2]);

            var birthDate = new DateTime(year, month, day);
            var today = DateTime.Now;

            int age = today.Year - birthDate.Year;
            if (birthDate > today.AddYears(-age)) age--;

            Console.WriteLine();

            Console.WriteLine($"{name} ist {age} Jahr{(age == 1 ? "" : "e")} alt.");
        }

        public override void SecondTask()
        {
            for (int i = 0; i < 5; i++)
            {
                _list.AddItem();
                Console.WriteLine();
            }

            Console.Write("Aktueller  Gesamtpreis: ");
            Message.Colored(_list.TotalPrice().ToString(), ConsoleColor.Red);
            Console.WriteLine("€");
        }

        public override void ThirdTask()
        {
            _list.ShowList();
        }

        public override void FourthTask()
        {
        }

        public override void FifthTask()
        {
        }
    }
}