// Created by Moritz on 10.02.2021

using System;
using System.Collections.Generic;

namespace School.Lesson
{
    public class LessonManager
    {
        private static LessonManager _instance;

        private List<Lesson> lessons;
        private Dictionary<int, Lesson> lessonHash;

        public LessonManager()
        {
            _instance = this;

            lessons = new();
            lessonHash = new();
        }


        public void RegisterLessons()
        {
            lessons.Add(new LessonOne());
            lessons.Add(new LessonTwo());

            foreach (var lesson in lessons)
                lessonHash.Add(lesson.GetCount(), lesson);
        }

        public Lesson GetLesson(int count)
        {
            return lessonHash.GetValueOrDefault(count, null);
        }

        public List<Lesson> GetLessons()
        {
            return lessons;
        }

        public static LessonManager GetInstance()
        {
            return _instance;
        }
    }
}