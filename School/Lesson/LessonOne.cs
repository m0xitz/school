// Created by Moritz on 10.02.2021

using System;

namespace School.Lesson
{
    public class LessonOne : Lesson
    {
        public LessonOne() : base("one", 1, new DateTime(2021, 2, 10))
        {
        }


        public override void FirstTask()
        {
            Console.WriteLine("Mein Name ist Moritz, ich bin 18 Jahre alt und spiele gerne Fußball.");
        }

        public override void SecondTask()
        {
            Console.WriteLine("     *");
            Console.WriteLine("    ***");
            Console.WriteLine("   *****");
            Console.WriteLine("  *******");
            Console.WriteLine(" *********");
            Console.WriteLine("***********");
            Console.WriteLine("     |");
        }

        public override void ThirdTask()
        {
            Console.WriteLine("Wie heißen Sie mit Vornamen?");
            string firstname = Console.ReadLine();
            Console.WriteLine("Wie heißen Sie mit Nachnamen?");
            string lastname = Console.ReadLine();

            Console.WriteLine("In welcher Straße wohnen Sie?");
            string street = Console.ReadLine();
            Console.WriteLine("Wie ist Ihre Hausnummer?");
            string number = Console.ReadLine();
            Console.WriteLine("Wie ist die Postleitzahl?");
            string postcode = Console.ReadLine();
            Console.WriteLine("In welcher Stadt wohnen Sie?");
            string city = Console.ReadLine();

            Console.Clear();
            Console.WriteLine($"{firstname} {lastname} wohnt in {city}.");
            Console.WriteLine("Die vollständige Adresse lautet:");
            Console.WriteLine($"{firstname} {lastname}");
            Console.WriteLine($"{street} {number}");
            Console.WriteLine($"{postcode} {city}");
        }

        public override void FourthTask()
        {
            Console.WriteLine("Geben Sie zwei Zahlen ein:");

            Console.Write("Erste Zahl: ");
            int x = Int32.Parse(Console.ReadLine() ?? "0");
            Console.Write("Zweite Zahl: ");
            int y = Int32.Parse(Console.ReadLine() ?? "0");

            int sum = x + y;
            int difference = x - y;
            int product = x * y;
            int quotient = x / y;

            Console.WriteLine();

            Console.WriteLine($"Addition: {x} + {y} = {sum}");
            Console.WriteLine($"Addition: {x} + {y} = {sum}");
            Console.WriteLine($"Subtraktion: {x} - {y} = {difference}");
            Console.WriteLine($"Multiplikation: {x} * {y} = {product}");
            Console.WriteLine($"Divsison: {x} / {y} = {quotient}");
        }

        public override void FifthTask()
        {
            Console.WriteLine("Geben Sie zwei Zahlen ein:");

            Console.Write("Erste Zahl: ");
            int x = Int32.Parse(Console.ReadLine() ?? "0");
            Console.Write("Zweite Zahl: ");
            int y = Int32.Parse(Console.ReadLine() ?? "0");

            int area = x * y;
            int scope = 2 * x + 2 * y;

            Console.WriteLine();

            Console.WriteLine($"Fläche: {area}");
            Console.WriteLine($"Umfang: {scope}");
        }
    }
}