// Created by Moritz on 10.02.2021

using System;
using Microsoft.VisualBasic;

namespace School.Lesson
{
    public abstract class Lesson
    {
        private string _name;
        private int _count;
        private DateTime _date;

        protected Lesson(string name, int count, DateTime date)
        {
            _name = name;
            _count = count;
            _date = date;

            Console.WriteLine($"Lesson '{name}' ({count}) from {date:dd.MM.yyyy} registered.");
        }

        public abstract void FirstTask();

        public abstract void SecondTask();

        public abstract void ThirdTask();

        public abstract void FourthTask();

        public abstract void FifthTask();

        public string GetName()
        {
            return _name;
        }

        public int GetCount()
        {
            return _count;
        }

        public DateTime GetDate()
        {
            return _date;
        }
    }
}