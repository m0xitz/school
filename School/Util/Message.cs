// Created by Moritz on 25.02.2021

using System;

namespace School.Util
{
    public class Message
    {
        public static void Colored(string message, ConsoleColor color)
        {
            var oldColor = Console.ForegroundColor;

            Console.ForegroundColor = color;
            Console.Write(message);
            Console.ForegroundColor = oldColor;
        }

        public static void Colored(string message, object arg0, ConsoleColor color)
        {
            var oldColor = Console.ForegroundColor;

            Console.ForegroundColor = color;
            Console.Write(message, arg0);
            Console.ForegroundColor = oldColor;
        }

        public static void Colored(string message, object arg0, object arg1, ConsoleColor color)
        {
            var oldColor = Console.ForegroundColor;

            Console.ForegroundColor = color;
            Console.Write(message, arg0, arg1);
            Console.ForegroundColor = oldColor;
        }

        public static void Colored(string message, object arg0, object arg1, object arg2, ConsoleColor color)
        {
            var oldColor = Console.ForegroundColor;

            Console.ForegroundColor = color;
            Console.Write(message, arg0, arg1, arg2);
            Console.ForegroundColor = oldColor;
        }

        public static string Colored(ConsoleColor color)
        {
            var oldColor = Console.ForegroundColor;

            Console.ForegroundColor = color;
            string input = Console.ReadLine();
            Console.ForegroundColor = oldColor;
            return input;
        }
    }
}