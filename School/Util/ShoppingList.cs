// Created by Moritz on 24.02.2021

using System;
using System.Collections.Generic;
using System.Linq;

namespace School.Util
{
    public class ShoppingList
    {
        private static ShoppingList _instance;

        private Dictionary<string, int> items;

        public ShoppingList()
        {
            _instance = this;

            items = new();
        }

        public void AddItem()
        {
            Console.Write("Item: ");
            string item = Message.Colored(ConsoleColor.Blue);

            Console.Write("Preis: ");
            int price = int.Parse(Message.Colored(ConsoleColor.Red) ?? "0");

            items.Add(item, price);

            Console.Write("Neues Item hinzugefügt: ");
            Message.Colored(item, ConsoleColor.Blue);
            Console.Write(" (");
            Message.Colored(price.ToString(), ConsoleColor.Red);
            Console.WriteLine("€)");
        }

        public void ClearList()
        {
            items.Clear();

            Message.Colored("Die Einkaufsliste wurde geleert.", ConsoleColor.Red);
            Console.WriteLine();
        }

        public void ShowList()
        {
            int count = items.Count;

            if (count == 0)
            {
                Message.Colored("Die Einkaufsliste ist leer.", ConsoleColor.Red);
                Console.WriteLine();
                return;
            }

            Console.Write("Die Einkaufsliste beinhaltet ");
            Message.Colored(count.ToString(), ConsoleColor.DarkCyan);
            Console.WriteLine($" Item{(count == 1 ? "" : "s")}.");
            Console.WriteLine();
            foreach (var (item, price) in items)
            {
                Console.Write("  - ");
                Message.Colored(item, ConsoleColor.Blue);
                Console.Write(" (");
                Message.Colored(price.ToString(), ConsoleColor.Red);
                Console.WriteLine("€)");
            }

            Console.WriteLine();
            Console.Write("Der Gesamtwarenwert beträgt ");
            Message.Colored(TotalPrice().ToString(), ConsoleColor.Red);
            Console.WriteLine("€.");
        }

        public int TotalPrice()
        {
            return items.Sum(item => item.Value);
        }

        public bool Empty()
        {
            return items.Count == 0;
        }

        public static ShoppingList GetInstance()
        {
            return _instance;
        }
    }
}